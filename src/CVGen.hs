{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module CVGen where

import Control.Monad
import Data.Aeson
import Data.Monoid
import Data.Aeson.Encode.Pretty
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Char8 as LB8
import qualified Data.HashMap.Strict as HM
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Vector as V
import Path
import Path.IO
import System.Exit
import System.FilePath (dropExtensions)
import qualified Text.Mustache as Mustache
import qualified Text.Mustache.Types as Mustache

autoDir :: Path Rel Dir
autoDir = $(mkRelDir "auto")

resultJsonCV :: Path Rel File
resultJsonCV = $(mkRelFile "resume.json")

cvgen :: IO ()
cvgen = do
    rootdir <- getCurrentDir
    cvgenIn rootdir

cvgenIn :: Path Abs Dir -> IO ()
cvgenIn rootdir = do
    jsonCV <- updateJSONCV rootdir
    renderCvsFromTemplates (addLengths jsonCV) rootdir

updateJSONCV :: Path Abs Dir -> IO Value
updateJSONCV rootdir = do
    jsonCV <- makeJsonCV rootdir
    let resultFile = rootdir </> autoDir </> resultJsonCV
    ensureDir $ parent resultFile
    LB.writeFile (toFilePath resultFile) $ encodePretty jsonCV
    pure jsonCV

manualDir :: Path Rel Dir
manualDir = $(mkRelDir "manual")

infoDir :: Path Rel Dir
infoDir = $(mkRelDir "info")

makeJsonCV :: Path Abs Dir -> IO Value
makeJsonCV rootdir = do
    (_, fs) <- listDir $ rootdir </> manualDir </> infoDir
    vs <-
        (catMaybes <$>) $
        forM fs $ \pieceF -> do
            let piece = dropExtensions . toFilePath . filename $ pieceF
            if piece == ""
                then pure Nothing
                else Just <$> do
                         contents <- LB.readFile $ toFilePath pieceF
                         case eitherDecode contents of
                             Left err ->
                                 die $
                                 unwords
                                     [ toFilePath pieceF
                                     , "could not be decoded:"
                                     , err
                                     ]
                             Right pieceV ->
                                 pure (T.pack piece .= (pieceV :: Value))
    pure $ object vs

addLengths :: Value -> Value
addLengths = go
  where
    go (Object o) =
        Object $
        mconcat $
        flip map (HM.toList o) $ \(key, value) ->
            (case value of
                 Array ls ->
                     HM.singleton (key <> "_length") (toJSON $ V.length ls) <>
                     HM.singleton (key <> "_empty") (toJSON $ V.null ls)
                 _ -> HM.empty) <>
            HM.singleton key (go value)
    go (Array ls) = Array $ V.map go ls
    go v = v

templatesDir :: Path Rel Dir
templatesDir = $(mkRelDir "templates")

genDir :: Path Rel Dir
genDir = $(mkRelDir "gen")

directiveFile :: Path Rel File
directiveFile = $(mkRelFile "directive.json")

renderCvsFromTemplates :: Value -> Path Abs Dir -> IO ()
renderCvsFromTemplates cv rootdir = do
    (_, fs) <- listDirRecur (rootdir </> manualDir </> templatesDir)
    let directiveFiles = filter ((== directiveFile) . filename) fs
    forM_ directiveFiles $ \df -> do
        contents <- LB.readFile $ toFilePath df
        case eitherDecode contents of
            Left err ->
                die $
                unwords
                    [ toFilePath df
                    , "could not be decoded to a template directive:"
                    , err
                    ]
            Right TemplateDirective {..} ->
                renderTemplateCv
                    cv
                    (parent df </> templateIn)
                    (rootdir </> autoDir </> genDir </> templateOut)

data TemplateDirective = TemplateDirective
    { templateIn :: Path Rel File
    , templateOut :: Path Rel File
    } deriving (Show, Eq)

instance FromJSON TemplateDirective where
    parseJSON (Object o) =
        TemplateDirective <$> o .: "template" <*> o .: "result"
    parseJSON _ = mempty

sharedDir :: Path Rel Dir
sharedDir = $(mkRelDir "shared")

renderTemplateCv :: Value -> Path Abs File -> Path Abs File -> IO ()
renderTemplateCv cv infile outfile = do
    templateOrErr <-
        Mustache.automaticCompile
            [ toFilePath $ parent infile
            , toFilePath $ parent (parent infile) </> sharedDir
            , toFilePath $ parent (parent (parent infile)) </> sharedDir
            ] $
        toFilePath infile
    case templateOrErr of
        Left err ->
            die $
            unwords
                [ "template"
                , toFilePath infile
                , "could not be compiled:"
                , show err
                ]
        Right template -> do
            let (errs, text) =
                    Mustache.checkedSubstituteValue template $
                    Mustache.mFromJSON cv
            case errs of
                [] -> do
                    ensureDir $ parent outfile
                    T.writeFile (toFilePath outfile) text
                _ ->
                    die $
                    unlines $
                    [ "Failed to substitute template"
                    , toFilePath infile
                    , "to make"
                    , toFilePath outfile
                    ] ++
                    map show errs ++
                    ["with context:", LB8.unpack $ encodePretty cv] ++
                    [ "Failed to substitute template"
                    , toFilePath infile
                    , "to make"
                    , toFilePath outfile
                    ] ++
                    map show errs
